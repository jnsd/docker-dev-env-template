# Docker Development Environment Template

Basic template to create and use a docker development environment (docker dev env). This enables you to
- quickly set up a docker dev env for developer teams
- enjoy stable, repeatable and team-consistent development conditions: no system and library dependency issues


## Initial Setup

Needs to be executed once at the beginning of your project.

1. Clone the repo: `git clone git@gitlab.com:jnsd/docker-dev-env-template.git`
2. Use the folder name of your project repo, e.g. `my-project`, to name the docker dev env: `./setup-dev-env.sh my-project`
3. Remove the `setup.sh` file
3. Copy the entire content to your project repo
4. Delete the docker dev env template repo
5. Go to the folder `docker-env` in your project repo
6. Run `./build.sh`

This is what you have now:
- `my-project-env`: docker image of your docker dev env
- `my-project-dev`: docker container with your project dev env


## Working with Your Docker Dev Env

Run `./run-dev.sh` in your project folder and execute everything within the just started container. You can edit project sources and git commit/pull/push from the host system. The docker dev env will forward any X11 output from the dev env container to your host system.


## Tech

The docker image is based on a minimal Ubuntu linux that is properly set up for running in a docker container: https://github.com/phusion/baseimage-docker

To share a project folder with the docker container, it needs to be passed with absolute paths: `docker run -v /absolute/host/path:/absolute/container/path`. Thus, this docker dev env expects the following conventions:

- your projects reside in `~/dev-share` on your host, e.g. `~/dev-share/my-project`
- the `build.sh` script creates a `/home/prj-repo` within the docker container
- the `run.sh` script mounts the host folder `~/dev-share/my-project` as `home/prj-repo` in the container

This allows to directly edit all project files on your host - using the tools of your choice - and directly affect the dev env that runs within the docker container.