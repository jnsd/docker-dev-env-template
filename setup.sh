project_name=$1

sed -i ".bk" "s/||project_name||/$project_name/g" run-dev.sh
rm run-dev.sh.bk

sed -i ".bk" "s/||project_name||/$project_name/g" docker-env/build.sh
rm docker-env/build.sh.bk
